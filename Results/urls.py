from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(r'^quality_control', views.quality_control),
    re_path(r'^embed_', views.embed),
]