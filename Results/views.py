from django.shortcuts import render
import os
import subprocess

from django.contrib.auth.decorators import login_required

PATH = "/opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/PROJECTS/"
PATH_RESULTS = "/opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/Pipeline/Results/templates/Results/"
project_selected = ""

# Create your views here.
@login_required(login_url="/usermanagement/login/")
def quality_control(request):
    if request.method=="POST":
        global project_selected
        #vars
        project_selected = request.POST.get("sel_project")
        fastqsR1 = request.POST.get("sel_R1_fastqs")
        fastqsR2 = request.POST.get("sel_R2_fastqs")
        bed = request.POST.get("sel_bed")
        genome = request.POST.get("sel_genome")
        parallel_processes = request.POST.get("sel_pp")

        path_project = PATH + project_selected
        path_bed = PATH + "INPUTS/" + bed
        #subprocess.call("sh " + PATH + "varCall_Pipeline2020.sh " + path_project + " " + fastqsR1 + " " + fastqsR2 + " " + path_bed, shell=True)
        html_files = list_files(".html", project_selected + "/fastqc")# get the list of .html files
        if (not os.path.exists(PATH_RESULTS + project_selected)):
            os.mkdir(PATH_RESULTS + project_selected)#create the directory that will contains all the main and all symlinks
            create_symbolic_link(project_selected, html_files)
            create_html_file(project_selected, html_files)
            create_embed_file(project_selected, html_files)
        args = {"project":path_project, "fastqsR1":fastqsR1, "fastqsR2":fastqsR2, "bed":path_bed}
        return render(request, 'Results/' + project_selected + "/quality_control_" + project_selected + '.html', args)
    else:
        args = {}
        return render(request, 'Results/quality_control.html', args)

def create_symbolic_link(project_selected: str, html_files: list):
    'function that creates symboliks links from the fastqc directory to the project'
    for f in html_files:
        # Source file path 
        src = PATH + project_selected + "/fastqc/" + f
        # Destination file path 
        dst = PATH_RESULTS + project_selected + "/" + f
        os.symlink(src, dst)

def create_html_file(project_name: str, html_files: list):
    'function that creates the main.html of the project'
    html_content = "{% extends 'base_results.html' %}"
    html_content += "{% block head %}<title> quality control " + project_name + "</title>{% endblock %}"
    html_content += "{% block body %}"
    html_content += "<div class='container-well'><table class='table table-bordered table-dark'><tr>"
    count = 0
    for f in html_files:
        if (count % 4 == 0 and count > 0):
            html_content += "</tr><tr>"
        html_content += "<td><small>" + f.replace(".html","") + "</small></td>"
        count += 1
    html_content += "</tr></table></div><br>"
    html_content += "<div><embed src='embed_" + project_name + ".html' width='90%' height='800px'></div>"
    html_content += "{% endblock %}"
    with open("Results/templates/Results/" + project_name + "/quality_control_" + project_name + ".html", 'w') as html_file:
        html_file.write(html_content)

def create_embed_file(project_name: str, html_files: list):
    'function that creates the embed.html of the main project'
    html_content = "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>"
    for f in html_files:
        html_content += "<div id='" + f.replace(".html","") + "'>{% include 'Results/David_Project/" + f + "' %}</div>"
    with open("Results/templates/Results/" + project_name + "/embed_" + project_name + ".html", 'w') as html_file:
        html_file.write(html_content)

def list_files(type_file: str, subpath: str) -> list:
    'function that allows to list all the files from a directory'
    files = []
    output = subprocess.Popen("ls " + PATH + subpath, shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    output_str = output_str.replace("b'","")
    output_str = output_str.replace("\\n'","") 
    file_list = output_str.split("\\n")
    for file_ in file_list: 
        if (file_.find(type_file) != -1) : 
            files.append(file_)
    return files

def embed(request):
    return render(request, "Results/" + project_selected + "/embed_" + project_selected + ".html", {})