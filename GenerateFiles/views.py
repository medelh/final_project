from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
import os
import subprocess

from django.contrib.auth.decorators import login_required

PATH = "/opt/Ngs_analysis/Sharedwww/Genomica_TOT/NGS/PROJECTS/"

# Create your views here.
@login_required(login_url="/usermanagement/login/")
def select_project(request):
    arr_files_subfiles = []
    fastq_list = []
    project_list = list_files("Project", "")
    for project in project_list:
        file_subfile = []
        fastq_list = list_files(".fastq", project + "/fastq")
        fastq_list.extend(list_files(".fq", project + "/fastq"))
        bed_list = list_files(".bed", project + "/INPUTS")
        file_subfile = [[project], fastq_list, bed_list]
        arr_files_subfiles.append(file_subfile)
        parallel_processes = range(1,11)
        args = {"projects":project_list, "arr_files_subfiles":arr_files_subfiles, "fastq_list":fastq_list, "parallel_processes": parallel_processes}
    return render(request, "GenerateFiles/select_project_and_options.html", args)

def list_files(type_file: str, subpath: str) -> list:
    'function that returns a list of all the files from a directory'
    files = []
    output = subprocess.Popen("ls " + PATH + subpath, shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    output_str = output_str.replace("b'","")
    output_str = output_str.replace("\\n'","") 
    file_list = output_str.split("\\n")
    for file_ in file_list: 
        if (file_.find(type_file) != -1) : 
            files.append(file_)
    return files