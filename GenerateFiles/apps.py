from django.apps import AppConfig


class GeneratefilesConfig(AppConfig):
    name = 'GenerateFiles'
