$(document).ready(function () {
    //general var
    arr_files_subfiles = []
    onload();

    // on change select project
    $("#select_project").change(function () {
        fill_fastq_and_bed_select()
        remove_fastq_selected()
        show_hide_errors()
        fill_post_data()
    });

    // select, unselect fastq files
    $("#btnAdd").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list")[0].length - 1; i >= 0; i--) {
            if ($("#select_fastq_list")[0][i].selected) {
                $("#select_fastq_list")[0][i].selected = false;
                $("#select_fastq_list_selected").append($("#select_fastq_list")[0][i]);
            }
        }
        fill_post_data();
    });

    $("#btnRemove").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list_selected")[0].length - 1; i >= 0; i--) {
            if ($("#select_fastq_list_selected")[0][i].selected) {
                $("#select_fastq_list_selected")[0][i].selected = false;
                $("#select_fastq_list").append($("#select_fastq_list_selected")[0][i]);
            }
        }
        fill_post_data();
    });

    $("#select_bed_list").change(function () {
        fill_post_data()
    });

    $("#select_genome").change(function () {
        fill_post_data()
    });

    $("#select_parallel_processes").change(function () {
        fill_post_data()
    });

});

//function onload, hide some elements, and call other functions
function onload() {
    $("#str_files_subfiles").hide();
    $('.hide_option_selected').hide();
    transform_str_files_subfiles_to_arr();
    fill_fastq_and_bed_select()
    show_hide_errors()
    fill_post_data()
}

//function that transforms the str_files_subfiles to array
function transform_str_files_subfiles_to_arr() {
    str_files_subfiles = $("#str_files_subfiles").text()
    str_files_subfiles = str_files_subfiles.replace("[[", "")
    str_files_subfiles = str_files_subfiles.replace("]]]", "")
    str_files_subfiles = str_files_subfiles.replace(/'/g, "")
    str_files_subfiles = str_files_subfiles.replace(/ /g, "")
    str_files_subfiles = str_files_subfiles.split("]],[")
    for (let i = 0; i < str_files_subfiles.length; i++) {
        arr_aux = str_files_subfiles[i].replace("[", "")
        arr_aux = arr_aux.split("],[")
        arr_aux_file_subfile = []
        for (let j = 0; j < arr_aux.length; j++) {
            arr_aux_file_subfile.push(arr_aux[j].split(","))
        }
        arr_files_subfiles.push(arr_aux_file_subfile)
    }
}

//function that fills the select with the options of the arr_files_subfiles
function fill_fastq_and_bed_select(){
    $('#select_fastq_list').find('option').remove();
    for (let i = 0; i < arr_files_subfiles.length; i++) {
        if ($("#select_project option:selected")[0].value == i) {
            for (let j = 0; j < arr_files_subfiles[i][1].length; j++) {
                if(arr_files_subfiles[i][1][j] != ""){
                    var o = new Option(arr_files_subfiles[i][1][j], arr_files_subfiles[i][1][j]);
                    $("#select_fastq_list").append(o);
                }
            }
            $('#select_bed_list').find('option').remove();
            for (let j = 0; j < arr_files_subfiles[i][2].length; j++) {
                if(arr_files_subfiles[i][2][j] != ""){
                    var o = new Option(arr_files_subfiles[i][2][j], arr_files_subfiles[i][2][j]);
                    $("#select_bed_list").append(o);
                }
            }
        }
    }
}

//function that removes the .fastq selected when change the project
function remove_fastq_selected(){
    $('#select_fastq_list_selected').find('option').remove();
}

//function that shows/hide some elements if the list of .fastqs and .bed files is void
function show_hide_errors(){
    if ($("#select_fastq_list")[0].length == 0){
        $("#div_fastqs").hide();
        $("#div_error_fastqs").show();
    }else{
        $("#div_fastqs").show();
        $("#div_error_fastqs").hide();
    }
    if ($("#select_bed_list")[0].length == 0){
        $("#div_bed").hide();
        $("#div_error_bed").show();
    }else{
        $("#div_bed").show();
        $("#div_error_bed").hide();
    }
}

//function that fills the span with the data of the select fastq_selected
function fill_post_data(){
    //fill input project selected
    $('#input_project_selected').val($('#select_project option:selected').text());
    //fill input fastq list selected
    var optionR1Values = [];
    var optionR2Values = [];
    $('#select_fastq_list_selected option').each(function() {
        if($(this).val().search("R1") >= 0){
            optionR1Values.push($(this).val());
        }else if($(this).val().search("R2") >= 0){
            optionR2Values.push($(this).val());
        }
    });
    $('#input_fastq_R1_selected').val(optionR1Values.toString());
    $('#input_fastq_R2_selected').val(optionR2Values.toString());
    //fill input bed selected
    $('#input_bed_selected').val($('#select_bed_list option:selected').text());
    //fill input genome selected
    $('#input_genome_selected').val($('#select_genome option:selected').text());
    //fill input paralleled processes selected
    $('#input_num_pp_selected').val($('#select_parallel_processes option:selected').text());
}