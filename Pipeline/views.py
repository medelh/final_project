from django.shortcuts import render
from django.shortcuts import redirect

from django.contrib.auth.decorators import login_required

# Create your views here.

def init(request):
    return redirect('/home/')

@login_required(login_url="/usermanagement/login/")
def home(request):
    args = {}
    return render(request, "Pipeline/home.html", args)