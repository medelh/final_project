from django.urls import path, re_path
from . import views
from django.contrib.auth import login, logout
from django.contrib.auth.views import LoginView

urlpatterns = [
    path('register/', views.register_page),
    path('login/', views.login_page),
    path('logout/', views.logout_user),
]